import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';
import { Observable } from 'rxjs';
import { IUser } from '../interfaces/user';
import { UrlBuilderService } from 'src/app/services/url-builder.service';

export interface ILoginInput {
  username: string,
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser: IUser;

  constructor(private req: RequestService, private url: UrlBuilderService) { }

  // incrementClickCounter(): void {
  //   this.clickCounter++;
  // }

  getUserData(): Observable<IUser> {
    return this.req.get<IUser>(this.url.getUrl('getUser', { userId: 'testing' }));
  }

  login(loginInput: ILoginInput): Observable<IUser> {
    return this.req.get<IUser>(this.url.getUrl('login', { ...loginInput }));
  }

}
