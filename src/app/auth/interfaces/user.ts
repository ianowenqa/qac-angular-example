export interface IUser {
    firstName: string;
    lastName: string;
    _id?: string;
    _v?: number;
}
