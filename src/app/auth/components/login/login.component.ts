import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { RequestService } from 'src/app/services/request.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginForm.valueChanges.subscribe((val) => {
      console.log('GROUP', val);
    });
  }

  loginFormSubmitted() {
    // this.auth.login()
  }

  clicked() {
    this.auth.getUserData()
      .subscribe((data) => {
        console.log('data', data);
      });

  }

}
