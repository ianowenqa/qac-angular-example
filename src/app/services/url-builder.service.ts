import { Injectable } from '@angular/core';
import { environment as conf } from '../../environments/environment';


export interface IEndpoints {
  login: string,
  getUser: string
}

export interface IUrlParams {
  [paramKey: string]: string | number | boolean 
}

export type EndpointKeys = keyof IEndpoints;

/**
 * Provides URL construction services to various parts of the application.
 * These URLs are normally consumed by other API calling services.
 *
 * @author ioawnen
 * @export
 * @class UrlBuilderService
 */
@Injectable({
  providedIn: 'root'
})
export class UrlBuilderService {

  
  constructor() { 
  }

  /**
   * Returns a constucted URL string for an input endpoint key.
   *
   * @param {EndpointKeys} key Endpoint url key
   * @param {Object} [urlParams={}] Object containing param key and value
   * @returns {string} Constructed URL
   * @author ioawnen
   * @memberof UrlBuilderService
   */
  getUrl(key: EndpointKeys, urlParams: IUrlParams = {}): string {
    const { baseUrl = '# ERR NO BASE URL FOUND #', endpoints = {} } = conf;
    let url = baseUrl + (endpoints[key] || '# ERR NO URI FOUND #');

    for (const paramKey in urlParams) {
      url = url.replace(`:${paramKey}`, urlParams[paramKey].toString());
    }

    return url;
  }


}
